<resources>
    <string name="app_name">Better schedule</string>
    <string name="app_moto">A faster schedule for Bakaláři. For students, by students.</string>
    <string name="start">Start</string>
    <string name="prev_week">Previous week</string>
    <string name="next_week">Next week</string>
    <string name="current_week">Current week</string>
    <string name="permanent_schedule">Permanent schedule</string>
    <string name="settings">Settings</string>
    <string name="refresh">Refresh</string>

    <string name="bakalari_logo">Logo of Bakaláři</string>
    <string name="username">Username</string>
    <string name="password">Password</string>
    <string name="school">School</string>
    <string name="choose_school">Select</string>
    <string name="change_school">Change</string>
    <string name="no_school_selected">Not selected</string>
    <string name="login_button">Login</string>
    <string name="login_message">Log in to Bakaláři, please</string>
    <string name="invalid_login">Invalid username or password</string>
    <string name="schedule_disabled">Schedule is disabled on this school</string>
    <string name="unexpected_response">Unexpected response from server</string>
    <string name="school_unreachable">School server unreachable</string>
    <string name="unreachable">Server unreachable (check address)</string>
    <string name="no_internet">No internet connection</string>
    <string name="unknown_error">An error occurred</string>
    <string name="enter_username">Enter your username</string>
    <string name="enter_password">Enter your password</string>
    <string name="enter_school">Select your school</string>
    <string name="search">Search or enter URL address…</string>
    <string name="schools_info_connection_failed">Downloading school list failed</string>
    <string name="loading_schools_info">Loading schools info…</string>
    <string name="schools_retry">Retry</string>
    <string name="use_url">Enter URL address manually</string>
    <string name="use_url_dialog_title">Enter server URL address</string>
    <string name="use_url_dialog_hint">e.g.: https://bakalari.school.com/bakaweb</string>

    <!-- Info messages -->
    <string name="info_last_week">Last week</string>
    <string name="info_this_week">This week</string>
    <string name="info_next_week">Next week</string>
    <string name="info_permanent">Permanent schedule</string>

    <string name="info_offline">%1$s (offline)</string> <!-- first argument is week "name" such as 'This week'. Result looks like this: 'This week (offline)'. Signal that user if offline-->
    <string name="info_unreachable">Error: connection failed</string> <!-- Same as 'info_offline', but when user uses refresh button -->
    <string name="info_login_failed">Error: login failed</string>
    <string name="info_unexpected_response">Error: unexpected server response</string> <!-- Same as 'info_offline', but when user uses refresh button -->
    <string name="info_app_error">Error: unknown</string>

    <string name="user">User</string>
    <string name="student">Student</string>
    <string name="teacher">Teacher</string>
    <string name="parent">Parent</string>
    <string name="logout">Logout</string>
    <string name="look_and_behaviour">App look &amp; behavior</string>
    <string name="info_line">Info-line</string>
    <string name="info_line_desc">Show information line</string>
    <string name="switch_to_next_week">Switch to the next week</string>
    <string name="center_to_current_lesson">Jump to the current lesson</string>
    <string name="notification">Persistent notification</string>
    <string name="notification_desc">Shows a silent notification with your next lesson</string>
    <string name="notification_detials">Persistent notification informs you about your current or next lesson during school. It begins to show the first lesson 1 hour before it starts and then switches to the next one always 10 minutes before the current lesson ends. After the school finishes the notification disappears.\n\nThis feature can be disabled in the settings.</string>
    <string name="notification_no_permission_title">Notifications are disabled</string>
    <string name="notification_no_permission">This app\'s notifications are blocked by Android. Please unblock them in device settings and try again.</string>
    <string name="support_this_app">Support developer</string>
    <string name="support_this_app_desc">Donate to support development of this app</string>
    <string name="supporting_this_app">You are a sponsor</string>
    <string name="supporting_this_app_desc">You already donated some money. Thank you!</string>
    <string name="dont_show_again">Don\'t show again</string>
    <string name="something_went_wrong">Something went wrong :(</string>
    <string name="report">Report</string>

    <string name="monday">Mon</string>
    <string name="tuesday">Tue</string>
    <string name="wednesday">Wed</string>
    <string name="thursday">Thu</string>
    <string name="friday">Fri</string>
    <string name="saturday">Sat</string>
    <string name="sunday">Sun</string>

    <!-- Theme preferences -->
    <string name="app_theme_screen">Theme settings</string>
    <string name="app_theme_screen_desc">Customize colors of this app</string>

    <string name="theme_general_settings">General settings</string>
    <string name="app_theme">App theme</string>
    <string name="more_themes">Get more themes</string>
    <string name="export_theme">Export theme</string>
    <string name="export_theme_desc">Generate a text containing your current theme</string>
    <string name="export_theme_detail">The following text contains all data about your current theme. Send it to your friends to share your amazing design skills with them!</string>
    <string name="import_theme">Import theme</string>
    <string name="import_theme_desc">Import theme from a text generated using the button above</string>
    <string name="import_theme_detail">Paste text generated using the \'export theme\' feature here. Get one from your friends or at %1$s</string>
    <string name="import_invalid">Invalid data. Make sure you haven\'t missed any letters at the end.</string>
    <string name="copy_to_clipboard">Copy</string>
    <string name="paste_from_clipboard">Paste from clipboard</string>
    <string name="share">Share</string>
    <string name="share_subject">My Better Schedule theme</string>
    <string name="share_instructions">I made this amazing theme for Better Schedule! Use the link below or paste it (make sure you paste the whole link) into \'Better Schedule &gt; Settings &gt; Theme settings &gt; Import theme\' to apply this theme.</string>
    <string name="share_via">Share…</string>
    <string name="import_button">Import</string>
    <string name="import_clear">Clear</string>
    <string name="import_edittext_hint">Paste here</string>

    <string name="theme_custom_settings">Custom theme settings</string>
    <string name="primary_color">Primary color</string>
    <string name="primary_color_desc">App bar, buttons,…</string>
    <string name="accent_color">Accent color</string>
    <string name="accent_color_desc">Switches, checkboxes,…</string>
    <string name="background_color">Background color</string>
    <string name="cells_background">Cell fill color</string>
    <string name="cell_background">Fill</string>
    <string name="cell_primary_t">Primary text</string>
    <string name="cell_primary_t_desc">Subject and day of week abbreviations</string>
    <string name="cell_secondary_t">Secondary text</string>
    <string name="cell_secondary_t_desc">Teacher abbreviation, date, lesson begin time,…</string>
    <string name="cell_room_t">Room text</string>
    <string name="cell_room_t_desc">Classroom number</string>
    <string name="type_normal_lesson">Normal lesson</string>
    <string name="type_change">Change</string>
    <string name="type_change_desc">Lesson canceled, moved, etc.</string>
    <string name="type_no_school">No school</string>
    <string name="type_no_school_desc">School trip, museum visit, etc.</string>
    <string name="type_empty">Empty cell</string>
    <string name="type_header">Headers</string>
    <string name="type_header_desc">Day and time cells</string>
    <string name="other">Other</string>
    <string name="divider_color">Divider color</string>
    <string name="divider_width">Divider width</string>
    <string name="highlight_color">Highlight color</string>
    <string name="highlight_color_desc">Highlight of current lesson</string>
    <string name="highlight_width">Highlight width</string>
    <string name="infoline_fill">Info-line fill</string>
    <string name="infoline_fill_desc">The little text on the bottom, if enabled</string>
    <string name="infoline_text_color">Info-line text color</string>
    <string name="infoline_text_size">Info-line text size</string>
    <string name="customize_less">Customize less details</string>
    <string name="customize_less_desc">Generates some colors automatically based on provided values</string>
    <string name="customize_more">Customize more details</string>
    <string name="customize_more_desc">You choose everything</string>
    <string name="not_a_float_error">Enter a decimal number like 4.2</string>
    <string name="homework_color">Homework dot color</string>
    <string name="homework_color_desc">Color of the little dot which marks that there is a homework for the lesson</string>
    <string name="homework_size">Homework dot size</string>

    <!--These are shown in the lesson details dialog-->
    <string name="lesson_teacher">Teacher:</string>
    <string name="subject_name">Subject name:</string>
    <string name="room">Room:</string>
    <string name="absence">Absence:</string>
    <string name="topic">Topic:</string>
    <string name="group">Group:</string>
    <string name="change">Change:</string>
    <string name="lesson_name">Name:</string>
    <string name="notice">Notice:</string>
    <string name="cycle">Cycle:</string>
    <string name="homework">Homework:</string>

    <string name="group_in_notification">group: %1$s</string>
    <string name="in">in</string>

    <string name="next_lesson">Next lesson</string>
    <string name="prev_lesson">Previous lesson</string>
    <string name="lesson_cancelled">Canceled</string>
    <string name="nothing">No lesson</string>

    <string name="close">Close</string>
    <string name="yes">Yes</string>
    <string name="no">No</string>
    <string name="ok">OK</string>
    <string name="cancel">Cancel</string>

    <string name="about">About</string>
    <string name="source_code">Source code</string>
    <string name="source_code_desc">Visit GitLab page</string>
    <string name="feedback">Send feedback</string>
    <string name="feedback_desc">Report a bug, ask a question, etc.</string>
    <string name="send_crash">Send crash reports</string>
    <string name="send_crash_desc">Automatically report crashes to the developers so that they can be fixed</string>
    <string name="welcome_send_crash">Automatically report crashes to the developers so that they can be fixed</string>
    <string name="privacy_policy">Privacy policy</string>
    <string name="oss_licences">Open source licenses</string>
    <string name="oss_licences_desc">Show licenses of open source libraries</string>
    <string name="app_version">App version</string>
    <string name="email_message">These are information about installation of this app. Please don\'t remove them - they are essential for troubleshooting your problem.</string>
    <string name="send_email">Send email…</string>
    <string name="no_email_client">There are no email clients installed.</string>
    <string name="copy_address">Copy address and message</string>
    <string name="copied_to_clipboard">Copied to clipboard</string>
    <string name="include_schedule">Include schedule?</string>
    <string name="include_schedule_desc">Include your schedule in the message?\n\nDefinitely choose \'yes\' if reporting a visual bug.</string>

    <string name="notification_channel_name">Permanent notification</string>

    <string name="crash_reports_not_allowed">Automatic crash reports are allowed only on official release builds.</string>

    <string name="small_widget_label">Current lesson</string>
    <string name="wide_widget_label">5-lesson overview</string>
    <string name="widget_style">Style</string>
    <string name="widget_logged_out">Logged out</string>
    <string name="widget_background">Background</string>
    <string name="widget_transparency">Transparency</string>
    <string name="widget_autotext">Automatic text color</string>
    <string name="widget_text_color">Text color</string>

    <string name="donate_title">Become a sponsor</string>
    <string name="donate_text1">Support app development and get some bonus features:</string>
    <string name="donate_bonus_features">- Custom widget color and transparency\n- Black theme\n- Custom themes\n- Any theme you find on the internet\n- You can share your amazing theme on the internet</string>
    <string name="donate_a_little">Donate a little (%s)</string>
    <string name="donate_a_little_no_price">Donate a little</string>
    <string name="donate_more">Donate more (%s)</string>
    <string name="donate_more_no_price">Donate more</string>
    <string name="donate_text2">Naturally, you get all the bonus features even for a small donation.</string>
    <string name="donate_title_ok">You are a sponsor!</string>
    <string name="donate_text1_ok">You have supported the app development and you have unlocked all the bonus features:</string>
    <string name="use_promo_code">Redeem code</string>
    <string name="restore_purchases">Restore purchases</string>
    <string name="restore_purchases_desc">Checks purchases on your Google account. Use if you did donate, but bonus features are still locked.</string>
    <string name="purchases_restored">Purchases restored</string>

    <string name="purchase_cancelled">Purchase canceled by user</string>
    <string name="purchase_error">Purchase failed (%s)</string>
    <string name="error_code">error %s</string>

    <!-- Day types-->
    <string name="day_type_workday"></string>
    <string name="day_type_holiday">Holiday</string>
    <string name="day_type_celebration">Public holiday</string>
    <string name="day_type_weekend">Weekend</string>
    <string name="day_type_director_day">Free day</string>

    <string name="app_updated">App has been updated</string>
    <string name="whats_new">What\'s new</string>
    <!-- Interesting feature messages-->
    <string name="interesting_themes">Customize this app using themes!</string>

    <string name="wtf_rozvrh_title">An error occurred</string>
    <string name="wtf_rozvrh_message">Your schedule could not be rendered correctly! Would you like to report this issue via email now? The problematic schedule will be included in the report.</string>
    <string name="wtf_rozvrh_report">Report</string>
    <string name="wtf_rozvrh_later">Not now</string>
    <string name="wtf_not_fot_month">Not for a month</string>

    <plurals name="info_weeks_back">
        <item quantity="one">%d week back</item>
        <item quantity="other">%d weeks back</item>
    </plurals>
    <plurals name="info_weeks_forward">
        <item quantity="one">%d week forward</item>
        <item quantity="other">%d weeks forward</item>
    </plurals>

</resources>
