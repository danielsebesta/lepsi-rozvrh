# Seznam změn / Changelog

## 1.8.8 (2. 8. 2023)

- Oprava drobné změny v API

---

- Fix API change

## 1.8.5 (12. 8. 2022)

- Opraveno vysoké využití baterie pokud školní server soustavně neodpovídá

---

- Fix battery drain when school server does not respond permanently

## 1.8.4 (21. 9. 2021)

- Opravena chyba při importu motivu

---

- Fix theme import

## 1.8.3 (17. 6. 2021)

- Opraveno nekonečné načítání

---

- Fixed infinite loading

## 1.8.2 (21. 5. 2021)

- Další drobné opravy

---

- More minor fixes

## 1.8.1 (29. 4. 2021)

- Drobné opravy

---

- Minor fixes

## 1.8 (25. 4. 2021)

- Zjednodušeno přihlašování
- Velké změny "pod pokličkou" - aplikace je nyní rychlejší a spolehlivější.

---

- Improved login screen
- Big changes under the hood - app is now faster and more reliable.

## 1.7.3 (12. 9. 2020)

- Opravy chyb

---

- Bug fixes

## 1.7.2 (2. 9. 2020)

- Opravy chyb

---

- Bug fixes

## 1.7.1 (1. 9. 2020)

- Provedena bleskurychlá oprava pro nové API v3 (protože staré přestalo fungovat)
- Zbytečné prázdné hodiny na začátku a na konci dne jsou nyní automaticky odstraněny.

---

- Very quick hotfix for the new API v3 (as the old API stopped working)
- Redundant empty lessons at the start of day are now automatically removed.

## 1.7 (7. 6. 2020)

- Vylepšen vzhled
- Přidány motivy
- Widget nyní může mít jakoukoliv barvu a průhlednost
- V rozvrhu jsou zvýrazněny domácí úkoly
- Můžete podpořit vývojáře prostřednictvím plateb Google Play.

---

- Improved app look
- Added themes
- Widget now has customizable color and transparency
- Homework is now highlighted in the schedule.
- You can now support the developer through Google Play purchases

## 1.6.1 (9. 3. 2020)

- Opravy chyb

---

- Bug fixes

## 1.6 (1. 3. 2020)

- Konečně přidán widget s tmavým nebo světlým vzhledem
- Přidán seznam změn přímo v aplikaci

---

- Finally added widget with light or dark theme
- Added changelog right in the app


## 1.5.3 (25. 1. 2020)

- Použita jiná knihovna pro zobrazování open source licencí.

---

- Changed the library for displaying open source licenses.

## 1.5.2 (25. 1. 2020)

- Opravy chyb
- Povoleno nezabezpečené spojení (http), protože některé školy stále nepoužívají http**s**.

---

- Bug fixes
- Allowed for unencrypted connection (http), because some schools still don't use http**s**.

## 1.5.1 (1. 1. 2020)

- Opravy chyb
- Tolerance opravdu divných rozvrhů, ve kterých hodiny "vylézají" z nadepsaných bloků.

---

- Bug fixes
- Very weird schedules with lessons "outside of" captions are tolerated.

## 1.5 (31. 12. 2019)

Verze 1.5 přináší několik nových funkcí a také hromadu vylepšení a oprav, zvláště pro učitele a uživatele tabletů.

- Učitelům se v rozvrhu místo jejich vlastního jména zobrazuje název třídy.
- Do trvalého upozornění byly přidány tlačítka na rychlé listování mezi hodinami.
- Výrazně zlepšeno vykreslování rozvrhu, zvláště na tabletech či velmi malých obrazovkách. Už žádné překrývající se nápisy či bílé pruhy napravo od rozvrhu.
- Upraveno rozložení nápisů, aby byl rozvrh přehlednější.
- A několik dalších drobných vylepšení či oprav...

---

Version 1.5 brings several new features and plenty of tweaks and fixes, especially for teachers and tablet users.

- Teachers now see the class name instead of their own name in the schedule.
- The permanent notification has buttons to browse through the following or past lessons.
- Massively improved rendering of the schedule, mainly on tablets and small screens. No more white stripes on the right of the schedule or texts overlapping.
- Improved cell layout to make the schedule less cluttered.
- And many small tweaks and bug fixes...

## 1.4 (9. 12. 2019)

- Přidána funkce trvalého upozornění, díky které máte Vaši příští hodinu vždy na očích.
- Drobné zkrášlení nastavení na starších zařízeních.

---

- Added a persistent notification feature, thanks to which you don't have to open the any more!
- Slight improvement of the look of settings on older devices.

## 1.3.2 (5. 12. 2019)

- Opravena chyba, která způsobovala pád aplikace při zobrazování zvláštních rozvrhů.

---

- Fixed a bug which caused the app to crash when showing a weird schedule.

## 1.3.1 (3. 12. 2019)

- Snížena zátěž na školní server.
- Zvýšena tolerance (hodně) pomalých severů.

---

- Optimized load on school server.
- Increased request time-out (for very slow servers).

## 1.3

- Při spuštění skočí rozvrh na aktuální hodinu.
- Změněna ikona pro zobrazení aktuálního týdne.
- Vylepšena funkce "Napsat autorovi".

---

- Schedule now scrolls to current lesson on app start.
- Changed icon for showing current week.
- Improved feedback button.

## 1.2

- Přidáno automatické hlášení pádů, pro zlepšení stability aplikace.
- Rozvrh při otočení obrazovky už neskočí na začátek.
- Opraven popis aplikace na F-Droidu.
- Drobné opravy.

---

- Added automatic crash reporting system for better stability of the app.
- Schedule no longer jumps to beginning when screen is rotated.
- Fixed description on F-Droid.
- Minor fixes.